package it.gl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.stepsrl.java.annotation.JWTAuth;

@JWTAuth
@RequestScoped
@Path("/rest")
public class SmsRes {

  @Context
  private UriInfo context;

  // range di generazione dell'id
  private final int MIN = 1;
  private final int MAX = 10000;
  
  // struttura temporanea per conservare gli sms creati
  static Map<Integer, SmsAck> createdSms = new HashMap<>();

  private int getId() {
    Random rand = new Random();
    int id = rand.nextInt((MAX - MIN) + 1) + MIN;
    return id;
  }

  // curl -H "Accept: application/json" localhost:8989/SmsRest/rest/sms/100
  
  @GET
  @Path("sms/{id}")
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response getSms(@PathParam("id") int id) {
    System.out.println("GET: " + id);

    SmsAck smsAck = createdSms.get(id);
    if (smsAck != null) {
      return Response.status(200).entity(smsAck).build();
    } else {
      return Response.status(404).entity("").build();
    }

  }

  // curl -H "Accept: application/json" localhost:8989/SmsRest/rest/sms
  @Path("sms")
  @GET
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response getAllSms() {

    List<SmsAck> allSms = new ArrayList<>();
    createdSms.forEach((id, sms) -> allSms.add(sms)); 
    GenericEntity<List<SmsAck>> list = new GenericEntity<List<SmsAck>>(allSms) { };
    //GenericEntity<Map<Integer, SmsAck>> list = new GenericEntity<Map<Integer, SmsAck>>(createdSms) { };
    return Response.status(200).entity(list).build();

   }
  
  // content JSON
  // curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d "{\"numTel\":\"335123456\",\"testoMsg\":\"Ciao, come stai?\" }" localhost:8989/SmsRest/rest/sms
  // curl -X POST -H "Accept: application/xml" -H "Content-Type: application/json" -d "{\"numTel\":\"335123456\",\"testoMsg\":\"Ciao, come stai?\" }" localhost:8989/SmsRest/rest/sms

  // content XML
  // curl -X POST -H "Accept: application/xml" -H "Content-Type: application/xml" -d "<SmsInfo><numTel>335123456</numTel><testoMsg>Ciao, come stai?</testoMsg></SmsInfo>" localhost:8989/SmsRest/rest/sms
  // curl -X POST -H "Accept: application/json" -H "Content-Type: application/xml" -d "<SmsInfo><numTel>335123456</numTel><testoMsg>Ciao, come stai?</testoMsg></SmsInfo>" localhost:8989/SmsRest/rest/sms
  @Path("sms")
  @POST
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response newSms(SmsInfo smsInfo) {
    System.out.println(smsInfo);

      String stato = "SMS creato. Invio fra un minuto";
      SmsAck smsAck = new SmsAck(getId(), smsInfo.getNumTel(),
          smsInfo.getTestoMsg(), stato);

      createdSms.put(smsAck.getId(), smsAck);

      return Response.status(201).entity(smsAck).build();

  }

  // curl -X DELETE localhost:8989/SmsRest/rest/sms/79
  @DELETE
  @Path("sms/{id}")
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response deleteSms(@PathParam("id") int id) {
    System.out.println("DELETE: " + id);

    SmsAck smsAck = createdSms.get(id);
    if (smsAck != null) {
      SmsAck deletedSMSAck = (SmsAck) smsAck.clone();
      createdSms.remove(id);
      System.out.println(deletedSMSAck);
      return Response.status(200).entity(smsAck).build();
    } else {
      return Response.status(404).entity("").build();
    }
 
  }

  // curl -X PUT -H "Accept: application/json" -H "Content-Type: application/xml" -d "<SmsInfo><numTel>335555555</numTel><testoMsg>Hi, come stai?</testoMsg></SmsInfo>" localhost:8989/SmsRest/rest/sms/6
  @PUT
  @Path("sms/{id}")
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response modifySms(@PathParam("id") int id, SmsInfo smsInfo) {
    System.out.println("PUT: " + id);
    System.out.println("PUT: " + smsInfo);

    if (!createdSms.containsKey(id)) {
      return Response.status(404).entity("").build();
    }
    
      String stato = "SMS modificato. Invio fra un minuto";
      SmsAck smsAck = new SmsAck(id, smsInfo.getNumTel(), smsInfo.getTestoMsg(),
          stato);
      createdSms.replace(id, smsAck);

      return Response.status(200).entity(smsAck).build();   
  }
  
}
