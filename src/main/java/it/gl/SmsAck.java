package it.gl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SmsAck")
@XmlAccessorType(XmlAccessType.FIELD)
public class SmsAck implements Cloneable {

  @XmlElement private int id;
  @XmlElement private String numTel;
  @XmlElement private String testoMsg;
  @XmlElement private String stato;
  @XmlElement private String data; 

  private String getDateTime() {
    
    // Get current date time
    LocalDateTime dataTime = LocalDateTime.now();
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    String formattedDateTime = dataTime.format(formatter);
    
    return formattedDateTime;
  }
  
  public SmsAck() { }
  
  public SmsAck(int id, String numTel, String testoMsg, String stato) {
    this.id = id;
    this.numTel = numTel;
    this.testoMsg = testoMsg;
    this.stato = stato;
    this.data = getDateTime();
  }

  @Override
  public Object clone() {
      try {
          return super.clone();
      }
      catch (CloneNotSupportedException e) {
          throw new Error("Errore clonazione oggetto");
      }
  }
  
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNumTel() {
    return numTel;
  }

  public void setNumTel(String numero) {
    this.numTel = numero;
  }

  public String getTestoMsg() {
    return testoMsg;
  }

  public void setTestoMsg(String testoMsg) {
    this.testoMsg = testoMsg;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "SmsAck [id=" + id + ", numTel=" + numTel + ", testoMsg=" + testoMsg + ", stato=" + stato + ", data=" + data
        + "]";
  }
  
}
