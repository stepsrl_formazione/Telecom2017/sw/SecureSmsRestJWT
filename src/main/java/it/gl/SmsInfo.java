package it.gl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SmsInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class SmsInfo {
  
  @XmlElement private String numTel;
  @XmlElement private String testoMsg;

  public SmsInfo() {
  }

  public SmsInfo(String numTel, String testoMsg) {
    this.numTel = numTel;
    this.testoMsg = testoMsg;
  }

  public String getNumTel() {
    return numTel;
  }

  public void setNumTel(String numero) {
    this.numTel = numero;
  }

  public String getTestoMsg() {
    return testoMsg;
  }

  public void setTestoMsg(String testoMsg) {
    this.testoMsg = testoMsg;
  }

  @Override
  public String toString() {
    return "SmsInfo [numTel=" + numTel + ", testoMsg=" + testoMsg + "]";
  }


}
