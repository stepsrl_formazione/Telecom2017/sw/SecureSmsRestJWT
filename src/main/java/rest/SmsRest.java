package rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.jboss.resteasy.plugins.interceptors.CorsFilter;
import org.stepsrl.java.rs.filters.JWTAuthFilter;
import org.stepsrl.java.rs.filters.JWTResponseFilter;

import it.gl.SmsRes;

//@ApplicationPath("/rest")
public class SmsRest extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> clazzes = new HashSet();
		clazzes.add(JWTAuthFilter.class);
		clazzes.add(SmsRes.class);
		clazzes.add(JWTResponseFilter.class);

		return clazzes;
	}

	 
}
