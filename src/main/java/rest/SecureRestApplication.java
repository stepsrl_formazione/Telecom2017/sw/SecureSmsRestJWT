package rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.jboss.resteasy.plugins.interceptors.CorsFilter;
import org.stepsrl.java.rs.AuthResource;

/**
 *
 * specific to /auth/token
 */
//@ApplicationPath("auth")
public class SecureRestApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> clazzes = new HashSet();
		clazzes.add(AuthResource.class);

		return clazzes;
	}

	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<Object>();
		CorsFilter corsFilter = new CorsFilter();
		corsFilter.getAllowedOrigins().add("*");
		corsFilter.setAllowedMethods("OPTIONS, GET, POST, DELETE, PUT, PATCH");
		singletons.add(corsFilter);

		return singletons;
	}

}
