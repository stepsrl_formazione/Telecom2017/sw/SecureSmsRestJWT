package rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.stepsrl.java.rs.AuthResource;
import org.stepsrl.java.rs.filters.JWTAuthFilter;
import org.stepsrl.java.rs.filters.JWTResponseFilter;

import it.gl.SmsRes;

@ApplicationPath("/")
public class RestApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> clazzes = new HashSet();
		
		clazzes.add(AuthResource.class);
		clazzes.add(JWTAuthFilter.class);
		clazzes.add(SmsRes.class);
		clazzes.add(JWTResponseFilter.class);

		return clazzes;
	}


	 
}
