package org.stepsrl.java.rs;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.stepsrl.java.annotation.JWTAuth;
import org.stepsrl.java.rs.util.JWTokenUtility;

/**
 *
 * just a dummy resource used to return the JWT token in response header
 */
@Path("/auth")
public class AuthResource {

    @Context
    SecurityContext sctx;

    @Path("/token")
    @POST
    @Produces("text/plain")
    public Response auth() {
        System.out.println("Authenticated user: " + sctx.getUserPrincipal().getName());

        //this.sctx = sctx;
        String authenticatedUser = sctx.getUserPrincipal().getName();
        Response resp = Response.ok(authenticatedUser + " authenticated")
                .header("jwt", JWTokenUtility.buildJWT(authenticatedUser))
                .build();

        return resp;
    }

}
